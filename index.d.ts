type NanobusEventListener = (...args: any[]) => void

interface Nanobus {
  emit(eventName: string, ...args: any[]): this
  on(eventName: string, listener: NanobusEventListener): this
  prependListener(eventName: string, listener: NanobusEventListener): this
  once(eventName: string, listener: NanobusEventListener): this
  prependOnceListener(eventName: string, listener: NanobusEventListener): this
  removeListener(
    eventName: string,
    listener: NanobusEventListener
  ): undefined | true
  removeAllListeners(eventName: string): this
  listeners(eventName: string): NanobusEventListener[]
}

interface NanobusConstructor {
  new (name?: string): Nanobus
  (name?: string): Nanobus
}

declare const Nanobus: NanobusConstructor

export = Nanobus
